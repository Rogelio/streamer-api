quick_flask_server==1.1.2
requests==2.26.0
flask_restx==0.5.1
pytz==2021.1
Flask==2.0.1
pymongo==3.12.0
