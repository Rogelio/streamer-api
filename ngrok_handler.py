import json

import requests


def get_tunnel_url():
    localhost_url = "http://localhost:4040/api/tunnels"
    tunnel_url = requests.get(localhost_url).text
    j = json.loads(tunnel_url)
    tunnel_url = j['tunnels'][0]['public_url']
    return tunnel_url
