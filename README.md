# Streamer-api
Get infos from Twitch streamers
## Features
![](features.png)

## Install
- Download and extract the project.
- Install [python3.8](https://www.python.org/downloads/) and [MongoDB Community Server](https://www.mongodb.com/try/download/community) on your system.
- Run `python3 -m pip install -r requirements.txt` to install the project dependencies.

## Usage
- Run a MongoDB server by opening a terminal, go to its installation folder then run the binary and give the path for the databse. On Windows it looks like this `.\mongod.exe --dbpath="c:\data\db"`
- You need to have an app registered on Twitch. The [official documentation](https://dev.twitch.tv/docs/api/#step-1-register-an-application) explains how to proceed.
- Retrieve your app' client_id and client_secret from [Twitch](https://dev.twitch.tv/console/apps).
- Decide of a random secret which length is comprised between 10 and 100 characters as detailed [here](https://dev.twitch.tv/docs/eventsub#secret).
- Set the following environment variables `CLIENT_ID=<CLIENT ID>`, `CLIENT_SECRET=<CLIENT_SECRET>`, `SUB_SECRET=<SUB_SECRET>` (the value after the `=` shouldn't be between `<>`)
- Run `python3 -m PATH/TO/main` by replacing `PATH/TO` by the actual absolute path to main.py.
- The ngrok dashboard is accessible at [http://localhost:4040](http://localhost:4040).
- The api dashboard is accessible at [http://localhost:8887/](http://localhost:8887/)
- In the console you also have the random ngrok tunnel URL that exposes the API. By default it gives you the http adress but the https is also aveilable.
- You souldn't need to route any port to expose the API.
