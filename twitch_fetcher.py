import hashlib
import hmac
import os

from ngrok_handler import *

CLIENT_ID = os.getenv("CLIENT_ID")
CLIENT_SECRET = os.getenv("CLIENT_SECRET")
SUB_SECRET = os.getenv("SUB_SECRET")


def get_access_token():
    r = requests.post(f'https://id.twitch.tv/oauth2/token?client_id={CLIENT_ID}'
                      f'&client_secret={CLIENT_SECRET}&grant_type=client_credentials')
    json_response = r.json()
    return json_response['access_token']


def get_streamer_infos(username):
    access_token = get_access_token()
    r = requests.get(f'https://api.twitch.tv/helix/users?login={username}',
                     headers={'Authorization': f'Bearer {access_token}', 'Client-Id': CLIENT_ID})
    json_response = r.json()
    streamers_data = {
        "platform": "Twitch",
        "username": username,
        "stream_url": f"twitch.tv/{username}",
        "profile_picture_url": json_response['data'][0]['profile_image_url']
    }
    return streamers_data


def get_streamer_id(username, access_token):
    r = requests.get(f'https://api.twitch.tv/helix/users?login={username}',
                     headers={'Authorization': f'Bearer {access_token}', 'Client-Id': CLIENT_ID})
    json_response = r.json()
    return json_response['data'][0]['id']


def get_stream_title(username):
    access_token = get_access_token()
    streamer_id = get_streamer_id(username, access_token)
    r = requests.get(f'https://api.twitch.tv/helix/channels?broadcaster_id={streamer_id}',
                     headers={'Authorization': f'Bearer {access_token}', 'Client-Id': CLIENT_ID})
    json_response = r.json()
    return json_response['data'][0]['title']


def sub_to_channel(username):
    access_token = get_access_token()
    streamer_id = get_streamer_id(username, access_token)
    ngrok_tunnel = get_tunnel_url()
    headers = {'Client-ID': CLIENT_ID, 'Authorization': f'Bearer {access_token}', 'Content-Type': 'application/json'}
    dic2encode = {
        "type": "stream.online",
        "version": "1",
        "condition": {
            "broadcaster_user_id": streamer_id
        },
        "transport": {
            "method": "webhook",
            "callback": f"{ngrok_tunnel}/subscriptions/callback",
            "secret": SUB_SECRET
        }
    }
    r = requests.post(
        headers=headers,
        data=json.dumps(dic2encode, indent=4),
        url="https://api.twitch.tv/helix/eventsub/subscriptions")
    return r.json()


def get_sub_list():
    access_token = get_access_token()
    headers = {'Client-ID': CLIENT_ID, 'Authorization': f'Bearer {access_token}'}
    r = requests.get(url=f"https://api.twitch.tv/helix/eventsub/subscriptions", headers=headers)
    return r.json()


def unsub_channel(sub_id, access_token):
    headers = {'Client-ID': CLIENT_ID, 'Authorization': f'Bearer {access_token}'}
    r = requests.delete(url=f"https://api.twitch.tv/helix/eventsub/subscriptions?id={sub_id}", headers=headers)
    return r


def unsub_all():
    sub_list = get_sub_list()['data']
    access_token = get_access_token()
    for sub in sub_list:
        unsub_channel(sub['id'], access_token)


def check_signature(headers, raw_body):
    twitch_signature = headers['Twitch-Eventsub-Message-Signature'].replace("sha256=", '')
    twitch_message_id = headers['Twitch-Eventsub-Message-Id'].encode()
    twitch_timestamp = headers['Twitch-Eventsub-Message-Timestamp'].encode()
    payload = twitch_message_id + twitch_timestamp + raw_body
    expected_signature = hmac.new(SUB_SECRET.encode(), payload, hashlib.sha256).hexdigest()
    return twitch_signature == expected_signature
