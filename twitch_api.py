from datetime import datetime

import pymongo
import pytz
from bson import json_util
from flask import Flask, request, Response
from flask_restx import fields, Api, Resource
from quick_flask_server import run_with_ngrok

from twitch_fetcher import *

mongo_client = pymongo.MongoClient("mongodb://localhost:27017/")
mongo_db = mongo_client["database"]
mongo_streamers = mongo_db["streamers"]
mongo_streams = mongo_db["streams"]

app = Flask(__name__)
run_with_ngrok(app)
api = Api(app=app, version='0.1', title='Twitch API', description='', validate=True)
streamer_definition = api.model('Streamer Informations', {'username': fields.String(required=True)})


def parse_json(data):
    return json.loads(json_util.dumps(data))


@api.route("/streamers/")
class StreamersList(Resource):
    @api.response(200, 'Success')
    def get(self):
        """
        Returns the list of streamers from the database
        """
        cursor = mongo_streamers.find({}, {"_id": 0, "username": 1})
        data = []
        for streamer in cursor:
            data.append(streamer['username'])
        return {"streamers": data}

    @api.response(200, 'Success')
    @api.response(403, 'Already exists')
    @api.response(404, 'Error')
    @api.expect(streamer_definition)
    def post(self):
        """
        Create a new streamer, provided stream username
        """
        data = request.get_json()
        if not data:
            data = {"response": "ERROR"}
            return data, 404
        else:
            username = data.get('username')
            if username:
                if mongo_streamers.find_one({"username": username}):
                    return {"response": "Streamer already exists."}, 403
                else:
                    streamer_data = get_streamer_infos(username)
                    mongo_streamers.insert_one(streamer_data)
                    return 200


@api.route("/streamers/<string:username>")
class Streamer(Resource):
    @api.response(200, 'Success')
    def get(self, username):
        """
        Returns the selected streamer from the database
        """
        return mongo_streamers.find_one({"username": username}, {"_id": 0})

    @api.response(200, 'Success')
    def delete(self, username):
        """
        Removes the selected streamer from the database
        """
        mongo_streamers.delete_one({'username': username})


@api.route("/streamers/<string:username>/streams")
class StreamerStreams(Resource):
    @api.response(200, 'Success')
    def get(self, username):
        """
        Returns the list of stream of a given streamer
        """
        cursor = mongo_streams.find({"username": username.lower()}, {"_id": 0})
        data = []
        for streamer in cursor:
            display_values = dict()
            display_values['title'] = streamer['stream_title']
            date_object = streamer['date']
            display_values['startDate'] = datetime.strftime(date_object, '%Y-%m-%d %H:%M:%S')
            display_values['startDateTs'] = int(datetime.timestamp(date_object))
            data.append(display_values)
        return {"streams": data}


@api.route("/subscriptions/callback")
class StreamsUpdate(Resource):
    @api.response(200, 'Success')
    @api.response(403, 'Invalid signature')
    def post(self):
        """
        Callback for twitch to tell us when a registered streamer goes live
        """
        data = request.get_json()
        if not check_signature(request.headers, request.get_data()):
            return data, 403
        if 'challenge' in data:
            response = Response(data['challenge'], status=200, headers={'content-length': ''}, mimetype="text/plain")
            return response
        elif 'event' in data:
            stream_data = dict()
            timestamp = data['event']['started_at']
            datetime_object = datetime.strptime(timestamp, '%Y-%d-%mT%H:%M:%SZ')
            stream_data['date'] = datetime_object.replace(tzinfo=pytz.utc)
            stream_data['username'] = data['event']['broadcaster_user_name']
            stream_data['stream_title'] = get_stream_title(stream_data['username'])
            mongo_streams.insert_one(stream_data)
            return 200


@api.route("/subscriptions/<string:username>")
class Subscriptions(Resource):
    @api.response(200, 'Success')
    @api.response(403, 'Already subscribed')
    def get(self, username):
        """
        Subscribes to the event handler for the given username
        """
        response = sub_to_channel(username)
        if 'status' in response and response['status'] == 409:
            return {"response": "Already subscribed to this streamer."}, 403
        else:
            return response