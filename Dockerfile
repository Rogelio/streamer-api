# syntax=docker/dockerfile:1

FROM python:3.8-slim-buster

WORKDIR /app

COPY requirements.txt requirements.txt
RUN pip3 install -r requirements.txt

COPY main.py main.py
COPY ngrok_handler.py ngrok_handler.py
COPY twitch_api.py twitch_api.py
COPY twitch_fetcher.py twitch_fetcher.py

CMD [ "python3", "-m" , "main.py", 'CLIENT_ID=<CLIENT_ID>', 'CLIENT_SECRET=<CLIENT_SECRET>','SUB_SECRET=<SUB_SECRET>']
